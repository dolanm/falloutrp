-- Advert Chat Type
nut.chat.register("advert", {
	onCanSay =  function(speaker, text)
		local char = speaker:getChar()
		return (char:hasMoney(10) and char:takeMoney(10))
	end,
	onCanHear = 1000000,
	onChatAdd = function(speaker, text)
		text = "ADVERT: " .. text
		chat.AddText(Color(180, 255, 10), text)
	end,
	prefix = {"/advert", "/ad"}
})


if (SERVER) then
	concommand.Add("forp-setup_ent_doors", function(client, command, arguments)
		if(!IsValid(client)) then
			if(!nut.plugin.list.doors) then
				return MsgN("[FalloutRP]: Door plugin missing in NutScript!")
			end

			local name = table.concat(arguments, " ")

			for _, entity in ipairs(ents.FindByClass("func_door")) do
				if(!entity:HasSpawnFlags(256) and !entity:HasSpawnFlags(1024)) then
					entity:setNetVar("noSell", true)
					entity:setNetVar("name", !name:find("%S") and "func_door" or name)
				end
			end

			nut.plugin.list.doors:SaveDoorData()

			MsgN("[FalloutRP]: Entity Doors have been set up.")
		end
	end)
end