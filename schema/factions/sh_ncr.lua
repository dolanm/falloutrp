FACTION.name = "New California Republic"
FACTION.desc = "Post-War republic from New California"
FACTION.color = Color(255, 191, 0)
FACTION.isDefault = false
FACTION.isGloballyRecognized = false
FACTION.models = {
    "models/player/ncr/female_01.mdl",
	"models/player/ncr/female_02.mdl",
	"models/player/ncr/female_03.mdl",
	"models/player/ncr/female_04.mdl",
	"models/player/ncr/ncr_01.mdl",
	"models/player/ncr/ncr_02.mdl",
	"models/player/ncr/ncr_03.mdl",
	"models/player/ncr/ncr_04.mdl",
	"models/player/ncr/ncr_05.mdl",
	"models/player/ncr/ncr_06.mdl",
	"models/player/ncr/ncr_07.mdl",
	"models/player/ncr/ncr_08.mdl",
	"models/player/ncr/ncr_09.mdl"
}
FACTION_NCR = FACTION.index