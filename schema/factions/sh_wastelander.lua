FACTION.name = "Wastelander"
FACTION.desc = "A factionless member of the wasteland"
FACTION.color = Color(255, 191, 0)
FACTION.isDefault = true
FACTION.isGloballyRecognized = false
FACTION.models = {
    "models/thespireroleplay/humans/group050/female_01.mdl",
	"models/thespireroleplay/humans/group050/female_01g.mdl",
	"models/thespireroleplay/humans/group050/female_02.mdl",
	"models/thespireroleplay/humans/group050/female_03.mdl",
	"models/thespireroleplay/humans/group050/female_04.mdl",
	"models/thespireroleplay/humans/group050/female_05.mdl",
	"models/thespireroleplay/humans/group050/female_06.mdl",
	"models/thespireroleplay/humans/group050/female_07.mdl",
	"models/thespireroleplay/humans/group050/female_08.mdl",
	"models/thespireroleplay/humans/group050/female_09.mdl",
	"models/thespireroleplay/humans/group050/female_10.mdl",
	"models/thespireroleplay/humans/group050/female_11.mdl",
	"models/thespireroleplay/humans/group050/female_12.mdl",
	"models/thespireroleplay/humans/group050/male_01.mdl",
	"models/thespireroleplay/humans/group050/male_01g.mdl",
	"models/thespireroleplay/humans/group050/male_02.mdl",
	"models/thespireroleplay/humans/group050/male_03.mdl",
	"models/thespireroleplay/humans/group050/male_04.mdl",
	"models/thespireroleplay/humans/group050/male_05.mdl",
	"models/thespireroleplay/humans/group050/male_06.mdl",
	"models/thespireroleplay/humans/group050/male_07.mdl",
	"models/thespireroleplay/humans/group050/male_08.mdl",
	"models/thespireroleplay/humans/group050/male_09.mdl",
	"models/thespireroleplay/humans/group050/male_10.mdl",
	"models/thespireroleplay/humans/group050/male_11.mdl",
	"models/thespireroleplay/humans/group050/male_12.mdl",
	"models/thespireroleplay/humans/group050/male_13.mdl",
	"models/thespireroleplay/humans/group050/male_14.mdl",
	"models/thespireroleplay/humans/group050/male_15.mdl",
	"models/thespireroleplay/humans/group050/male_16.mdl",
	"models/thespireroleplay/humans/group050/male_17.mdl",
	"models/thespireroleplay/humans/group050/male_18.mdl"
}
FACTION_WASTELANDER = FACTION.index