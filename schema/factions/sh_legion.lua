FACTION.name = "Caesar's Legion"
FACTION.desc = "Post-War dictatorship from Arizona"
FACTION.color = Color(160, 8, 7)
FACTION.isDefault = false
FACTION.isGloballyRecognized = false
FACTION.models = {
    "models/player/recruit.mdl"
}
FACTION_LEGION = FACTION.index