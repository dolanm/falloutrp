FACTION.name = "Enclave Remnants"
FACTION.desc = "Pre-War remnants of the United States Government"
FACTION.color = Color(17, 68, 113)
FACTION.isDefault = false
FACTION.isGloballyRecognized = false
FACTION_ENCLAVE = FACTION.index