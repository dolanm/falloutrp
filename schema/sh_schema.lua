SCHEMA.name = "Fallout Roleplay"
SCHEMA.author = "Dolan"
SCHEMA.desc = "A nutscript schema built for Fallout Roleplay"


if (SERVER) then

    resource.AddFile("sound/falloutrp/i/npc_human_using_stimpak.wav")
    resource.AddFile("materials/falloutrp/logo.png")
    resource.AddFile("materials/falloutrp/background.png")
    resource.AddFile("fonts/monofonto.ttf")

    resource.AddWorkshop("207739713") -- Nutscript Content

    resource.AddWorkshop("2077736447") -- FalloutRPG: reModels
    resource.AddWorkshop("2077720925") -- FalloutRPG: reMisc
    resource.AddWorkshop("2077712615") -- FalloutRPG: reMaterials
    resource.AddWorkshop("2001326388") -- FalloutRPG Map

    resource.AddWorkshop("109643223") -- 3D2D Textscreens
    resource.AddWorkshop("1912204263") -- Fallout: New Vegas - Faction Props
    resource.AddWorkshop("691812012") -- Fallout 4 - Workstation Props
    resource.AddWorkshop("1158858580") -- Fallout 4 - Wooden Build Kit
    resource.AddWorkshop("740183685") -- Fallout 4 - Vendor Stand Props
    resource.AddWorkshop("1647310022") -- Fallout 4 - Fortification Props
    resource.AddWorkshop("672365314") -- Fallout 4 - Aid and Chem Props
    resource.AddWorkshop("2093902215") -- Fallout: Mojave Wasteland
    resource.AddWorkshop("194459123") -- T-45D Power Armor Pack
    resource.AddWorkshop("193521626") -- T-51B Power Armor Pack
    resource.AddWorkshop("183739185") -- Fallout Ragdolls
    resource.AddWorkshop("834259345") -- Fixed Fallout Playermodels
    resource.AddWorkshop("220336312") -- PermaProps
    resource.AddWorkshop("105955548") -- No Collide Everything
    resource.AddWorkshop("212709244") -- Teleporter STool
    resource.AddWorkshop("207948202") -- Simple ThirdPerson - Sliders & Fixes!
    resource.AddWorkshop("415143062") -- TFA Base [ Reduxed ]
    resource.AddWorkshop("866368346") -- TFA INS Shared Attachments
    resource.AddWorkshop("2220262827") -- [TFA] Fallout Weapons Project
    resource.AddWorkshop("852046490") -- Fallout Models part 1 of 6
    resource.AddWorkshop("852055398") -- Fallout Models part 2 of 6
    resource.AddWorkshop("852058544") -- Fallout Models part 3 of 6
    resource.AddWorkshop("852062110") -- Fallout Models part 4 of 6
    resource.AddWorkshop("852066695") -- Fallout Models part 5 of 6
    resource.AddWorkshop("852068381") -- Fallout Models part 6 of 6
    resource.AddWorkshop("1756472849") -- [Fallout] Gannon Family Tesla Armour | Advanced Power Armour
    resource.AddWorkshop("1401013689") -- [F:NV] Legate Lanius [PM]
    resource.AddWorkshop("1894642111") -- [F:NV] NCR Veteran Ranger PM/Ragdoll
    resource.AddWorkshop("1748457033") -- Fallout Factions: Caesar's Legion Playermodels
    resource.AddWorkshop("1592997942") -- [TheHub] NCR Model Pack
    resource.AddWorkshop("1593008761") -- [TheHub] Material base
    resource.AddWorkshop("1783983829") -- [TheHub] Legion Model Pack
    resource.AddWorkshop("163522381") -- Fallout: New Vegas Securitron
    resource.AddWorkshop("2111702789") -- [TFA] Fallout Weapons Collection
    resource.AddWorkshop("392466182") -- Fallout fortifications
    resource.AddWorkshop("390803763") -- Fallout Clutter
    resource.AddWorkshop("1549590774") -- [TheHub] The Enclave
    resource.AddWorkshop("1499280228") -- [TheHub] Brotherhood Of Steel
    resource.AddWorkshop("1617010774") -- [TheHub] The Enclave Model Pack
    resource.AddWorkshop("1500471406") -- [TheHub] New California Republic
    resource.AddWorkshop("1526859722") -- [TheHub] Extra Model Pack
    resource.AddWorkshop("1474905782") -- [TheHub] Content Model Pack 1
    resource.AddWorkshop("1474934478") -- [TheHub] Content Model Pack 4
    resource.AddWorkshop("1474936923") -- [TheHub] Content Model Pack 5
    resource.AddWorkshop("1474915594") -- [TheHub] Content Model Pack 2
    resource.AddWorkshop("1478805935") -- [TheHub] Content Model Pack 6
    resource.AddWorkshop("1541747265") -- [TheHub] Extra Model Pack 2
    resource.AddWorkshop("1490431015") -- [TheHub] Content Model Pack 7
    resource.AddWorkshop("1492159943") -- [TheHub] Content Model Pack 8
    resource.AddWorkshop("1500453789") -- [TheHub] Citizens and Raiders
    resource.AddWorkshop("1612334174") -- [TheHub] BoS Model Pack
    resource.AddWorkshop("1644434267") -- [TheHub] Extra Content
    resource.AddWorkshop("1783783512") -- [TheHub] TFA Melee

    resource.AddWorkshop("1571918906") -- LFS Base
    resource.AddWorkshop("2087761012") -- LFS Vertibird
end

function SCHEMA:GetGameDescription()
    return "development"
end

function SCHEMA:LoadFonts(font, genericFont)
    surface.CreateFont("FNV", {
        font = "Monofonto", --  Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	    extended = false,
	    size = 32,
	    weight = 500,
	    blursize = 0,
	    scanlines = 0,
	    antialias = true,
	    underline = false,
	    italic = false,
	    strikeout = false,
	    symbol = false,
	    rotary = false,
	    shadow = false,
	    additive = false,
	    outline = false,
    })
end

nut.util.include("sh_configs.lua")
nut.util.include("cl_effects.lua")
nut.util.include("sv_hooks.lua")
nut.util.include("sh_hooks.lua")
nut.util.include("sh_commands.lua")
nut.util.include("meta/sh_player.lua")
nut.util.include("meta/sh_entity.lua")
nut.util.include("meta/sh_character.lua")
nut.util.include("sh_dev.lua") -- Developer Functions

nut.currency.set("", "cap", "caps")

nut.anim.setModelClass("models/adi/apaarmor1.mdl", "player")
nut.anim.setModelClass("models/adi/apatesla1.mdl", "player")
nut.anim.setModelClass("models/yates/rangercombat.mdl", "player")
nut.anim.setModelClass("models/yates/rangercombatf.mdl", "player") 
nut.anim.setModelClass("models/hub/armors/midwest.mdl", "player") 
nut.anim.setModelClass("models/hub/fallout/armors/chefchef.mdl", "player")