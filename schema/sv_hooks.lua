-- On character is created, Give him some money and items. 
function SCHEMA:OnCharCreated(client, id)
	local char = nut.char.loaded[id]

	if (char) then
		local inv = char:getInv()

		char:giveMoney(nut.config.get("startMoney", 0))
	end
end

function SCHEMA:EntityTakeDamage(target, dmgInfo)
	if(target:IsPlayer()) then
		local char = target:getChar()
		if(char) then
			local items = char:getInv():getItems()
			local armorItem = nil
			for k, v in pairs(items) do
				if(v:getData("equip")) then
					armorItem = v
				end
			end
			if(armorItem ~= nil) then
				if(dmgInfo:IsFallDamage()) then
					if(armorItem.hasFallResistance) then
						return true
					end
				else
					dmgInfo:ScaleDamage(1.0 - armorItem.damageResistance)
				end
			end
		end
	end
end

function SCHEMA:CanPlayerInteractItem(client, action, item)
	if(action == "drop" or action == "take") then
		return
	end

	local itemTable
	if (type(item) == "Entity") then
		if (IsValid(item)) then
			itemTable = nut.item.instances[item.nutItemID]
		end
	else
		itemTable = nut.item.instances[item]
	end

	if (itemTable and itemTable.needsSkill) then
		local reqattribs = ITEM_REQSKILLS[itemTable.uniqueID]
		
		if (reqattribs) then
			for k, v in pairs(reqattribs) do
				local attrib = client:getChar():getAttrib(k, 0)
				if (attrib < v) then
					client:notify("You are missing the " .. nut.attribs.list[k].name .. " attribute")

					return false
				end
			end
		end
	end
end