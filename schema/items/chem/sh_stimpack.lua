ITEM.name = "Stimpack"
ITEM.desc = "A type of hand-held medication used for boosting the body's own regenerative properties."
ITEM.model = "models/mosi/fallout4/props/aid/stimpak.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.category = "Chem"
ITEM.price = 200

ITEM.functions.use = {
    name = "Use",
    tip = "useTip",
    icon = "icon16/add.png",
    onRun = function(item)
        item.player:SetHealth(math.min(item.player:Health() + 25, 100))
        item.player:EmitSound("falloutrp/i/npc_human_using_stimpak.wav", 80)
    end
}