ITEM.name = "Anti-Material Rifle"
ITEM.desc = "The anti-materiel rifle is a left-handed, bolt-action rifle that uses .50 caliber rounds."
ITEM.model = "models/illusion/fwp/w_amr.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.category = "Weapon_Primary"
ITEM.weaponCategory = "primary"
ITEM.class = "tfa_fwp_amr"
ITEM.price = 100000