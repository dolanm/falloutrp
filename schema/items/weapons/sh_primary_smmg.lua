ITEM.name = "Shoulder Mounted Machine Gun (SMMG)"
ITEM.desc = "The shoulder mounted machine gun is essentially a shoulder-mounted minigun."
ITEM.model = "models/weapons/fml/fallout/c_smmg.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.category = "Weapon_Primary"
ITEM.weaponCategory = "primary"
ITEM.class = "tfa_fallout_smmg"
ITEM.price = 75000

ITEM.needsSkill = true