ITEM.name = "Tommy Gun"
ITEM.desc = "The Thompson M1928 submachine gun is a sinister looking weapon; every time you hold it, you have an urge to put on a fedora hat and crack your knuckles. The Thompson is well-fed by a large 50 round drum magazine. The Thompson is pretty powerful, but fires nondescript .45 caliber rounds. "
ITEM.model = "models/illusion/fwp/w_tommygun.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.category = "Weapon_Secondary"
ITEM.weaponCategory = "secondary"
ITEM.class = "tfa_fwp_tommygun"
ITEM.price = 750
--Note