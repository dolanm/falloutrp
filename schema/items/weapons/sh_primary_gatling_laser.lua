ITEM.name = "Gatling Laser"
ITEM.desc = "The Gatling laser uses multiple rotating barrels to fire a heavy and impressive stream of light radiation laser beams without overheating quickly."
ITEM.model = "models/fallout/vanilla_weps/c_gattlinglaser.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.category = "Weapon_Primary"
ITEM.weaponCategory = "primary"
ITEM.class = "tfa_fallout_gatlinglaser"
ITEM.price = 1500000

ITEM.needsSkill = true