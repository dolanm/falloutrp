ITEM.name = "Hunting Rifle"
ITEM.desc = "Hunting rifles are weapons designed for use in hunting. After the Great War, their accuracy and power allowed them to be adapted for hunting non-traditional types of game as well."
ITEM.model = "models/illusion/fwp/w_huntingrifle.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.category = "Weapon_Primary"
ITEM.weaponCategory = "primary"
ITEM.class = "tfa_fwp_huntingrifle"
ITEM.price = 750