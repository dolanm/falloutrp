ITEM.name = "Secondary Weapon Base"
ITEM.model = ""
ITEM.width = 2
ITEM.height = 2
ITEM.desc = "Base for secondary weapon items"
ITEM.category = "Weapon_Secondary"

local function onUse(item)
    item.player:EmitSound("items/gunpickup2.wav")
end

ITEM:hook("use", onUse)

ITEM.functions.use = {
    name = "Equip",
    tip = "useTip",
    icon = "icon16/add.png",
    onRun = function(item)
        --Maybe works?
    end,
}