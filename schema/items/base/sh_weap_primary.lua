ITEM.name = "Primary Weapon Base"
ITEM.model = ""
ITEM.width = 2
ITEM.height = 2
ITEM.desc = "Base for primary weapon items"
ITEM.category = "Weapon_Primary"

local function onUse(item)
    item.player:EmitSound("items/gunpickup2.wav")
end

ITEM:hook("use", onUse)

ITEM.functions.use = {
    name = "Equip",
    tip = "useTip",
    icon = "icon16/add.png",
    onRun = function(item)
        --Maybe works?
    end,
}