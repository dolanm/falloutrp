ITEM.name = "Legionary Legate Armor"
ITEM.desc = "Armor given to the legate of Caesar's Legion"
ITEM.category = "Armor"
ITEM.model = "models/fallout/apparel/legatearmor_go.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.clothingCategory = "model"
ITEM.pacData = {}
ITEM.replacements = "models/player/legate.mdl"
ITEM.price = 100

ITEM.damageResistance = 0.5
ITEM.hasFallResistance = false

function ITEM:getDesc()
    local description = self.desc .. "\n\nSTATS:\n- Damage Resistance: " .. (self.damageResistance * 100) .. "%\n- Fall Resistance: " .. tostring(self.hasFallResistance)
    return description
end