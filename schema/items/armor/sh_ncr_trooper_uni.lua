ITEM.name = "NCR Trooper Uniform"
ITEM.desc = "NCR Uniform and Helmet that is typically worn by NCR Troopers"
ITEM.category = "Armor"
ITEM.model = "models/fallout/apparel/trooper.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.clothingCategory = "model"
ITEM.pacData = {}
ITEM.replacements = {
	{"models/thespireroleplay/humans/group050/female_01.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_01g.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_02.mdl", "models/ncr/female_02.mdl"},
	{"models/thespireroleplay/humans/group050/female_03.mdl", "models/ncr/female_03.mdl"},
	{"models/thespireroleplay/humans/group050/female_04.mdl", "models/ncr/female_04.mdl"},
	{"models/thespireroleplay/humans/group054/female_05.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_06.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_07.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_08.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_09.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_10.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_11.mdl", "models/ncr/female_01.mdl"},
	{"models/thespireroleplay/humans/group050/female_12.mdl", "models/ncr/female_01.mdl"},

	{"models/thespireroleplay/humans/group050/male_01.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_01g.mdl", "models/player/ncr/hub/ncr_ghoul.mdl"},
	{"models/thespireroleplay/humans/group050/male_02.mdl", "models/ncr/ncr_02.mdl"},
	{"models/thespireroleplay/humans/group050/male_03.mdl", "models/ncr/ncr_03.mdl"},
	{"models/thespireroleplay/humans/group050/male_04.mdl", "models/ncr/ncr_04.mdl"},
	{"models/thespireroleplay/humans/group050/male_05.mdl", "models/ncr/ncr_05.mdl"},
	{"models/thespireroleplay/humans/group050/male_06.mdl", "models/ncr/ncr_06.mdl"},
	{"models/thespireroleplay/humans/group050/male_07.mdl", "models/ncr/ncr_07.mdl"},
	{"models/thespireroleplay/humans/group050/male_08.mdl", "models/ncr/ncr_08.mdl"},
	{"models/thespireroleplay/humans/group050/male_09.mdl", "models/ncr/ncr_09.mdl"},
	{"models/thespireroleplay/humans/group050/male_10.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_11.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_12.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_13.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_14.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_15.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_16.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_17.mdl", "models/ncr/ncr_01.mdl"},
	{"models/thespireroleplay/humans/group050/male_18.mdl", "models/ncr/ncr_01.mdl"}
}
ITEM.price = 100

ITEM.damageResistance = 0.3
ITEM.hasFallResistance = false

function ITEM:getDesc()
    local description = self.desc .. "\n\nSTATS:\n- Damage Resistance: " .. (self.damageResistance * 100) .. "%\n- Fall Resistance: " .. tostring(self.hasFallResistance)
    return description
end