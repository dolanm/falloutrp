ITEM.name = "Enclave Remnant Power Armor"
ITEM.desc = "A worn suit of power armor used by Enclave Remnants"
ITEM.category = "Armor"
ITEM.model = "models/fallout/apparel/adpowerarmor.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.clothingCategory = "model"
ITEM.pacData = {}
ITEM.replacements = "models/adi/apaarmor1.mdl"
ITEM.price = 85000

ITEM.damageResistance = 0.9
ITEM.hasFallResistance = true
ITEM.needsSkill = true

function ITEM:getDesc()
    local description = self.desc .. "\n\nSTATS:\n- Damage Resistance: " .. (self.damageResistance * 100) .. "%\n- Fall Resistance: " .. tostring(self.hasFallResistance)
    return description
end