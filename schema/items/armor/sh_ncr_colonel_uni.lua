ITEM.name = "NCR Colonel Armor"
ITEM.desc = "Armor given to the colonel of New California Republic"
ITEM.category = "Armor"
ITEM.model = "models/thespireroleplay/items/clothes/group056.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.clothingCategory = "model"
ITEM.pacData = {}
ITEM.replacements = "models/player/ncr/hub/ncrheavysgt.mdl"
ITEM.price = 100

ITEM.damageResistance = 0.5
ITEM.hasFallResistance = true

function ITEM:getDesc()
    local description = self.desc .. "\n\nSTATS:\n- Damage Resistance: " .. (self.damageResistance * 100) .. "%\n- Fall Resistance: " .. tostring(self.hasFallResistance)
    return description
end