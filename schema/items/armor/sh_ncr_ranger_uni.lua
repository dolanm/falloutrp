ITEM.name = "NCR Ranger Combat Armor"
ITEM.desc = "Usually worn by the NCR Rangers"
ITEM.category = "Armor"
ITEM.model = "models/fallout/apparel/combatranger.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.clothingCategory = "model"
ITEM.pacData = {}
ITEM.replacements = {
	{"models/thespireroleplay/humans/group050/female_01.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_01g.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_02.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_03.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_04.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_05.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_06.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_07.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_08.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_09.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_10.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_11.mdl", "models/yates/rangercombatf.mdl"},
	{"models/thespireroleplay/humans/group050/female_12.mdl", "models/yates/rangercombatf.mdl"},

	{"models/thespireroleplay/humans/group050/male_01.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_01g.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_02.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_03.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_04.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_05.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_06.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_07.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_08.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_09.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_10.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_11.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_12.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_13.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_14.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_15.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_16.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_17.mdl", "models/yates/rangercombat.mdl"},
	{"models/thespireroleplay/humans/group050/male_18.mdl", "models/yates/rangercombat.mdl"}
}
ITEM.price = 100

ITEM.damageResistance = 0.45
ITEM.hasFallResistance = false

function ITEM:getDesc()
    local description = self.desc .. "\n\nSTATS:\n- Damage Resistance: " .. (self.damageResistance * 100) .. "%\n- Fall Resistance: " .. tostring(self.hasFallResistance)
    return description
end