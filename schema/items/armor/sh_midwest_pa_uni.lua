ITEM.name = "★Midwestern Brotherhood Power Armor★"
ITEM.desc = "The armor is a self-contained suit of advanced technology armor. Powered by a micro-fusion reactor, with enough fuel to last a hundred years."
ITEM.category = "Armor"
ITEM.model = "models/fallout/apparel/power_armor.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.clothingCategory = "model"
ITEM.pacData = {}
ITEM.replacements = "models/hub/armors/midwest.mdl"
ITEM.price = 100

ITEM.damageResistance = 0.5
ITEM.hasFallResistance = false
ITEM.needsSkill = true

function ITEM:getDesc()
    local description = "\n\nSTATS:\n- Damage Resistance: " .. (self.damageResistance * 100) .. "%\n- Fall Resistance: " .. tostring(self.hasFallResistance)
    return description
end