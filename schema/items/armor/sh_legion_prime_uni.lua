ITEM.name = "Prime Legionary Armor"
ITEM.desc = "Armor given to all prime members of Caesar's Legion"
ITEM.category = "Armor"
ITEM.model = "models/fallout/apparel/legiongo.mdl"
ITEM.width = 2
ITEM.height = 2
ITEM.clothingCategory = "model"
ITEM.pacData = {}
ITEM.replacements = {
	{"models/thespireroleplay/humans/group050/male_01.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_01g.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_02.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_03.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_04.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_05.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_06.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_07.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_08.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_09.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_10.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_11.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_12.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_13.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_14.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_15.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_16.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_17.mdl", "models/player/war.mdl"},
	{"models/thespireroleplay/humans/group050/male_18.mdl", "models/player/war.mdl"}
}
ITEM.price = 100

ITEM.damageResistance = 0.3
ITEM.hasFallResistance = false

function ITEM:getDesc()
    local description = self.desc .. "\n\nSTATS:\n- Damage Resistance: " .. (self.damageResistance * 100) .. "%\n- Fall Resistance: " .. tostring(self.hasFallResistance)
    return description
end