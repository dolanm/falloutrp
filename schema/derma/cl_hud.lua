function SCHEMA:HUDPaint()
	local pl = LocalPlayer()
	local client = pl
	local char = client:getChar()

	if(!char) then
		return
	else
		local health = client:Health()
		local ap = client:getLocalVar("stm", 0)

		draw.RoundedBox(0, 90, ScrH() - 104, 302, 18, Color(0, 0, 0))
		draw.RoundedBox(0, 92, ScrH() - 102, health * 3 - 2, 14, nut.config.get("color"))

		draw.RoundedBox(0, ScrW() - 394, ScrH() - 104, 302, 18, Color(0, 0, 0))
		draw.RoundedBox(0, ScrW() - 392, ScrH() - 102, 298, 14, nut.config.get("color"))
		draw.RoundedBox(0, ScrW() - 392, ScrH() - 102, (100 - ap) * 3, 14, Color(0, 0, 0))
		
		draw.SimpleText("HP", "FNV", 67, 984, nut.config.get("color"), 1, 1)
		draw.SimpleText("AP", "FNV", 618 * 3, 984, nut.config.get("color"), 1, 1)
	end
end