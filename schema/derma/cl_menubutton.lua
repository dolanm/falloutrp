local PANEL = {}

function PANEL:Init()
    self:SetFont("FNV")
    self:SetExpensiveShadow(2, Color(0, 0, 0, 200))
    self:SetTextColor(nut.config.get("color"))
    self:SetDrawBackground(false)
    self.OldSetTextColor = self.SetTextColor
    self.SetTextColor = function(this, color)
        this:OldSetTextColor(color)
        this:SetFGColor(color)
    end
end

function PANEL:setText(text, noTranslation)
    surface.SetFont("FNV")

    self:SetText(noTranslation and text or L(text))

    if(!noTranslation) then
        self:SetToolTip(L(text.."Tip"))
    end

    local w, h = surface.GetTextSize(self:GetText())
    self:SetSize(w + 32, h + 24)
end

function PANEL:OnCursorEntered()
    local color = self:GetTextColor()
    local w, h = surface.GetTextSize(self:GetText())
    surface.PlaySound("ui/buttonrollover.wav")
end

function PANEL:OnCursorExited()

end

function PANEL:OnMousePressed(code)
    if(self.color) then
        self:SetTextColor(self.color)
    else
        self:SetTextColor(nut.config.get("color"))
    end

    surface.PlaySound("ui/buttonclickrelease.wav")

    if(code == MOUSE_LEFT and self.DoClick) then
        self:DoClick(self)
    end
end

function PANEL:OnMouseReleased(key)
    if(self.color) then
        self:SetTextColor(self.color)
    else
        self:SetTextColor(nut.config.get("color"))
    end
end

vgui.Register("frpMenuButton", PANEL, "DButton")