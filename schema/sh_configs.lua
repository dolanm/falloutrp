ITEM_REQSKILLS = {}

local function addRequiredSkill(itemID, reqAttribs)
    ITEM_REQSKILLS[itemID] = reqAttribs
end

addRequiredSkill("enclave_apa", {patrainingskill = 1})
addRequiredSkill("enclave_tesla", {patrainingskill = 1})
addRequiredSkill("midwest_pa_uni", {patrainingskill = 1})

addRequiredSkill("primary_gatling_laser", {strongback = 1})
addRequiredSkill("primary_smmg", {strongback = 1})