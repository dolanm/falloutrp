PLUGIN.name = "Securitron Gate Greeter"
PLUGIN.author = "Dolan"
PLUGIN.desc = "Adds NPC securitrons to process strip entry."

if (SERVER) then
    local PLUGIN = PLUGIN

    function PLUGIN:SaveData()
        local data = {}
        for k, v in ipairs(ents.FindByClass("nut_greeter")) do
            data[#data + 1] = {
                name = v:getNetVar("name"),
                desc = v:getNetVar("desc"),
                pos = v:GetPos(),
				angles = v:GetAngles(),
                model = v:GetModel(),
                scale = v.scale
            }
        end
        self:setData(data)
    end

    function PLUGIN:LoadData()
        for k, v in ipairs(ents.FindByClass("nut_greeter")) do
            v:Remove()
        end
        for k, v in ipairs(self:getData() or {}) do
            local entity = ents.Create("nut_greeter")
            entity:SetPos(v.pos)
            entity:SetAngles(v.angles)
            entity:Spawn()
            entity:SetModel(v.model)
            entity:setNetVar("name", v.name)
            entity:setNetVar("desc", v.desc)

            entity.scale = v.scale or 0.5
        end
    end
end