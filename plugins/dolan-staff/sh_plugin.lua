local PLUGIN = PLUGIN
PLUGIN.name = "Staff command addons"
PLUGIN.author = "Dolan"
PLUGIN.desc = "Additional staff commands that are borderline required for servers"

nut.command.add("devtest", {
	onRun = function(client, arguments)
		--succ me
	end
})

nut.command.add("plyrespawn", {
	adminOnly = true,
	syntax = "<string name>",
	onRun = function(client, arguments)
		local target = nut.command.findPlayer(client, arguments[1])

		if(IsValid(target) and target:getChar()) then
			target:Spawn()
			client:notify("You have respawned " .. target:Name())
			target:notify("You have been respawned")
		end
	end
})

nut.command.add("class", {
	onRun = function(client, arguments)
		if(IsValid(client) and client:getChar()) then
			local clientClass = client:getChar():getClass()

			if(nut.class.list[clientClass]) then
				local className = nut.class.list[clientClass].name
				client:notify("Current class: " .. className)
			else
				client:notify("Invalid class found, contact staff (" .. clientClass .. ")")
			end
		end
	end
})

nut.command.add("charsetclass", {
	adminOnly = true,
	syntax = "<string name> <string class>",
	onRun = function(client, arguments)
		local target = nut.command.findPlayer(client, arguments[1])

		if(IsValid(target) and target:getChar()) then
			local num = isnumber(tonumber(arguments[2])) and tonumber(arguments[2]) or -1

			if(nut.class.list[num]) then
				local newClass = nut.class.list[num]

				if(target:getChar():joinClass(num)) then
					target:notify("Your class has been updated to: " .. newClass)
					client:notify("Updated target class to: " .. newClass)
				else
					client:notify("Failed to update target class to: " .. newClass)
				end
			else
				for k, v in ipairs(nut.class.list) do
					if(nut.util.stringMatches(v.uniqueID, arguments[2]) or nut.util.stringMatches(L(v.name, target), arguments[2])) then
						if(target:getChar():joinClass(k)) then
							target:notify("Your class has been updated to: " .. v.name)
							client:notify("Updated target class to: " .. v.name)
						else
							client:notify("Failed to update target class to: " .. v.name)
						end
					end
				end
			end
		end
	end
})